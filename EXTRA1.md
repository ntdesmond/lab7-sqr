# Extra part 1

To avoid repetitive transactions, we could add a transaction ID generation in the code.

After committing a transaction, we would need to store the ID generated for the transaction.
Whenever the request with the same transaction ID appears, we check whether this ID has been already stored.
If so — the transaction was already committed and there is no need to redo it.