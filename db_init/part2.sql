\c lab

CREATE TABLE Inventory (
    username TEXT,
    product TEXT,
    amount INT CHECK (amount >= 0 AND amount <= 100),
    CONSTRAINT inventory_pk PRIMARY KEY (username, product)
);

CREATE OR REPLACE FUNCTION check_inventory_size()
    RETURNS TRIGGER
    LANGUAGE plpgsql
    AS $$
    DECLARE
        max_size CONSTANT INT := 100;
        current_amount CONSTANT INT := (
            SELECT sum(amount)
            FROM Inventory
            WHERE username = NEW.username AND product != NEW.product
        );
    BEGIN
        IF (current_amount + NEW.amount) > max_size THEN
            RAISE check_violation
                USING MESSAGE = format('Inventory max size (%s) exceeded', max_size);
        END IF;
        RETURN NULL;
    END;
$$;

CREATE TRIGGER inventory_size
    AFTER INSERT OR UPDATE ON Inventory
    FOR EACH ROW EXECUTE PROCEDURE check_inventory_size();

-- добавим жвачки по рублю
INSERT INTO Shop (product, in_stock, price) VALUES ('ruble_gum', 100, 1);