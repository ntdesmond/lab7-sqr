import psycopg2
from psycopg2.errors import CheckViolation

price_request = "SELECT price FROM Shop WHERE product = %(product)s"
buy_decrease_balance = f"UPDATE Player SET balance = balance - ({price_request}) * %(amount)s WHERE username = %(username)s"
buy_decrease_stock = "UPDATE Shop SET in_stock = in_stock - %(amount)s WHERE product = %(product)s"

# part 2
add_to_inventory = """
INSERT INTO Inventory (username, product, amount)
    VALUES (%(username)s, %(product)s, %(amount)s)
    ON CONFLICT ON CONSTRAINT inventory_pk
    DO UPDATE SET amount = Inventory.amount + EXCLUDED.amount
"""


def get_connection():
    return psycopg2.connect(
        dbname="lab",
        user="postgres",
        password="fucksqr",
        host="localhost",
        port=5432
    )  # TODO add your values here


def buy_product(username, product, amount):
    obj = {"product": product, "username": username, "amount": amount}
    with get_connection() as conn:
        with conn.cursor() as cur:
            try:
                cur.execute("BEGIN")
                cur.execute(buy_decrease_balance, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong username")

                cur.execute(buy_decrease_stock, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong product or out of stock")

                cur.execute(add_to_inventory, obj)
                if cur.rowcount != 1:
                    raise Exception("Congrats, you've hacked the inventory. Are you happy?")
            except CheckViolation as e:
                cur.execute("ROLLBACK")
                if "balance" in e.pgerror:
                    message = "Bad balance"
                elif "in_stock" in e.pgerror:
                    message = "Product is out of stock"
                else:
                    message = e.pgerror
                raise Exception(message) from e


buy_product('Alice', 'marshmello', 1)

# Part 2
buy_product('Bob', 'marshmello', 3)
buy_product('Bob', 'ruble_gum', 100)  # inventory size violation

